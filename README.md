# Simpli Films Hierarchy Tree 

This is a simple project test for and MCTEKK interview

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes. 


### Installing
Setup database credentials in the .env.example and rename it to .env:
```
DB_DATABASE=DatabaseName
DB_USERNAME=Username
DB_PASSWORD=Password
```
Install dependencies:
```
composer install
```

```
npm install
```
Run migration and seeds to the database:
```
php artisan migrate --seed
```

Serve the app:
```
php artisan serve
```
Available addresses in browser:
```
http://127.0.0.1:8000
```
JSON endpoint:
```
http://127.0.0.1:8000/json
```

Admin Dashboard:

```
http://127.0.0.1:8000/admin/
username: max@mctekk.com
password: 123456
```


## Authors

* **Angel Leger** - *for mctekk interview* - 