<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'created_at' => '2018-10-06 15:44:06',
                'email' => 'max@mctekk.com',
                'email_verified_at' => NULL,
                'id' => 1,
                'name' => 'Angel Leger',
                'password' => '$2y$10$d4djFMxt0pcVoo5NIynETuDMqcjf9AdEt/TwdimnwbXDMWuKBjKdW',
                'remember_token' => 'lXRQbwYfVxBfulsR7ccO28OkwYDDHn5CA5xKWocuipp9jpiCDw38il9uxPtR',
                'updated_at' => '2018-10-06 15:44:06',
            ),
        ));
        
        
    }
}