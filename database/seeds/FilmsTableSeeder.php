<?php

use Illuminate\Database\Seeder;

class FilmsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('films')->delete();
        
        \DB::table('films')->insert(array (
            0 => 
            array (
                'created_at' => '2018-10-06 23:26:34',
                'episodes' => 22,
                'id' => 1,
                'name' => 'Naruto',
                'parent_id' => 0,
                'release_date' => '2001-01-01 00:00:00',
                'season_id' => 1,
                'type' => 'serie',
                'updated_at' => '2018-10-06 23:26:34',
            ),
            1 => 
            array (
                'created_at' => '2018-10-22 15:18:35',
                'episodes' => 24,
                'id' => 2,
            'name' => 'Naruto Movie 1: Dai Katsugeki!! Yuki Hime Shinobu Houjou Dattebayo! (anime)',
                'parent_id' => 1,
                'release_date' => '2018-10-22 15:17:19',
                'season_id' => 1,
                'type' => 'serie',
                'updated_at' => '2018-10-22 15:18:35',
            ),
            2 => 
            array (
                'created_at' => '2018-10-22 15:30:40',
                'episodes' => 245,
                'id' => 4,
            'name' => 'Naruto OVA 3: Dai Katsugeki!! Yuki Hime Shinobu Houjou Dattebayo! Special: Konoha Annual Sports Festival (anime)',
                'parent_id' => 2,
                'release_date' => '2018-10-22 15:29:06',
                'season_id' => 4,
                'type' => 'serie',
                'updated_at' => '2018-10-22 15:30:40',
            ),
            3 => 
            array (
                'created_at' => '2018-10-22 15:39:43',
                'episodes' => 245,
                'id' => 6,
            'name' => 'Naruto OVA 1: Find the Crimson Four-leaf Clover! (anime)',
                'parent_id' => 1,
                'release_date' => '2018-10-22 15:29:06',
                'season_id' => 4,
                'type' => 'serie',
                'updated_at' => '2018-10-22 15:39:43',
            ),
            4 => 
            array (
                'created_at' => '2018-10-22 15:40:25',
                'episodes' => 245,
                'id' => 8,
            'name' => 'Naruto OVA 1: Find the Crimson Four-leaf Clover! (anime) sdfsd',
                'parent_id' => 1,
                'release_date' => '2018-10-22 15:29:06',
                'season_id' => 4,
                'type' => 'serie',
                'updated_at' => '2018-10-22 15:40:25',
            ),
            5 => 
            array (
                'created_at' => '2018-10-22 16:20:20',
                'episodes' => 245,
                'id' => 10,
            'name' => 'Naruto OVA 1: Find the Crimson Four-leaf Clover! (anime) sdfsdsds',
                'parent_id' => 1,
                'release_date' => '2018-10-22 15:29:06',
                'season_id' => 4,
                'type' => 'serie',
                'updated_at' => '2018-10-22 16:20:20',
            ),
            6 => 
            array (
                'created_at' => '2018-10-22 18:23:41',
                'episodes' => NULL,
                'id' => 11,
                'name' => 'neuvo',
                'parent_id' => 2,
                'release_date' => '2018-10-22 18:23:24',
                'season_id' => NULL,
                'type' => 'movie',
                'updated_at' => '2018-10-22 18:23:41',
            ),
            7 => 
            array (
                'created_at' => '2018-10-23 16:33:25',
                'episodes' => NULL,
                'id' => 12,
                'name' => 'Hijo de nuevo',
                'parent_id' => 11,
                'release_date' => '2018-10-23 16:33:14',
                'season_id' => NULL,
                'type' => 'movie',
                'updated_at' => '2018-10-23 16:33:25',
            ),
        ));
        
        
    }
}