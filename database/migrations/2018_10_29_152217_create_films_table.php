<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFilmsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('films', function(Blueprint $table)
		{
			$table->timestamps();
			$table->integer('episodes')->nullable()->index('films_episode_id_index');
			$table->increments('id');
			$table->string('name')->unique();
			$table->integer('parent_id')->unsigned()->default(0)->index();
			$table->dateTime('release_date');
			$table->integer('season_id')->unsigned()->nullable()->index();
			$table->string('type');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('films');
	}

}
