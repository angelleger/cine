<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Film;


class HomeController extends Controller
{

    public function show()
    {

        $films = Film::orderBy('release_date')->get();
        $array  = array();

        foreach ($films as $film){

            if ( $film['parent_id'] == 0)
                $film['parent_id']  = null;

               $array[$film['id']]  =  array('id' => $film['id'], 'name' => $film['name'], 'parent_id' => $film['parent_id'] );

       }

       $films = $this->makeNested($array);


        return view('home',compact('films'));
    }

    public function json()
    {
        $films = Film::orderBy('release_date')->get();
        $array  = array();

        foreach ($films as $film){

            if ( $film['parent_id'] == 0)
                $film['parent_id']  = null;

            $array[$film['id']]  =  array('id' => $film['id'],'name' => $film['name'], 'parent_id' => $film['parent_id'] );

        }

        $films = $this->makeNested($array);



        return response()->json(compact('films'), 200);


    }

    function makeNested($source) {
        $nested = array();

        foreach ( $source as &$s ) {
            if ( is_null($s['parent_id']) ) {
                // no parent_id so we put it in the root of the array
                $nested[] = &$s;

            }
            else {
                $pid = $s['parent_id'];
                if ( isset($source[$pid]) ) {
                    // If the parent ID exists in the source array
                    // we add it to the 'children' array of the parent after initializing it.

                    if ( !isset($source[$pid]['children']) ) {
                        $source[$pid]['children'] = array();
                    }

                    $source[$pid]['children'][] = &$s;
                }
            }
        }
        return $nested;
    }

}