<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\TemporadaRequest as StoreRequest;
use App\Http\Requests\TemporadaRequest as UpdateRequest;

/**
 * Class TemporadaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class TemporadaCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Temporada');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/temporada');
        $this->crud->setEntityNameStrings('temporada', 'temporadas');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        // add asterisk for fields that are required in TemporadaRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');


        $venue = [  // Select2
            'label' => "Serie",
            'type' => 'select2',
            'name' => 'serie_id', // the db column for the foreign key
            'entity' => 'serie', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Serie" // foreign key model
        ];
        $this->crud->addField($venue, 'update/create/both');


        $this->crud->addColumn([
            'label' => 'Serie',
            'type' => 'select',
            'name' => 'serie_id', // the db column for the foreign key
            'entity' => 'serie', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Serie" // foreign key model
        ]);


    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
