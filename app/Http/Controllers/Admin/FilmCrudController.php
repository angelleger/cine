<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\FilmRequest as StoreRequest;
use App\Http\Requests\FilmRequest as UpdateRequest;
use App\Models\Film;
/**
 * Class FilmCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class FilmCrudController extends CrudController
{
    public function setup()
    {
        $filmsNames = Film::pluck('name', 'id');
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Film');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/film');
        $this->crud->setEntityNameStrings('film', 'films');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        // add asterisk for fields that are required in FilmRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $parent = [
            'label' => "Episodes",
            'name' => 'episodes', // the db column for the foreign key
            'type' => 'number',


        ];
        $this->crud->addField($parent, 'update/create/both');

        $release_date = [
            'label' => "Release Date",
            'name' => 'release_date', // the db column for the foreign key
            'type' => 'datetime',
            'default' => date("Y-m-d H:i:s"),


        ];
        $this->crud->addField($release_date, 'update/create/both');

        $parent = [
            'label' => "Parent",
            'name' => 'parent_id', // the db column for the foreign key
            'entity' => 'film', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Film", // foreign key model,
            'allows_null' => true,
            'type' => 'select_from_array',
            'options' => $filmsNames,
            'default' => 'none',
        ];
        $this->crud->addField($parent, 'update/create/both');


        $type = [
            'name' => 'type',
            'label' => "Type",
            'type' => 'select_from_array',
            'options' => ['serie' => 'Serie', 'movie' =>'Movie'],
            'allows_null' => false,
            'default' => 'one',
            'hide_when' => [
                "Serie" => ['season_id', 'Season'],
            ],
        ];
        $this->crud->addField($type, 'update/create/both');


    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
       if( $request->parent_id == null){

           Request::merge(['parent_id' => 0]);

       }
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
